# APES HomeWork-3

This repository contains pthread practice with linked list implementation.
One thread takes a .txt file as input and computes the alphabets which are repeated exactly three times. Alphabets are logged to user given file.
Another thread logs the CPU utilization at every 100ms using posix timers.

Submitted as APES HomeWork-3
