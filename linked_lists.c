
/*******************************************************************************************************
*
* UNIVERSITY OF COLORADO BOULDER
*
* @file linked_lists.c
* @brief implementation of single linked list and data computation
* 
* This file implements the linked list to compute the alphabets in a file and 
* logs it to a file
*
* @author Kiran Hegde
* @date  2/18/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header Files
*
********************************************************************************************************/
#include <stdio.h>
#include <pthread.h>
#include "linked_lists.h"
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <sys/syscall.h>
#include <signal.h>


/********************************************************************************************************
*
* Mutex decalaration
*
********************************************************************************************************/
pthread_mutex_t mutex5 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex6 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex7 = PTHREAD_MUTEX_INITIALIZER;


/********************************************************************************************************
*
* global declaration of variables
*
********************************************************************************************************/
struct timespec t1, t2;
FILE *fp_log, *fp_data;
struct thread_info *my_struct;
struct node *list_node[NUM_ALPHA];
uint8_t i;

/********************************************************************************************************
*
* @name free_function
* @brief free the memory
*
* This function checls if a memory has been allocated, if yes, it destroys it
*
* @param None
*
* @return None
*
********************************************************************************************************/
void free_function()
{
	for(i=0; i<NUM_ALPHA; i++)
        {
		if(list_node[i])
                	free(list_node[i]);
        }
}

/********************************************************************************************************
*
* @name start_time
* @brief get current time
*
* This function gets the current time with
* a resolution of nanoseconds and logs it to th file
*
* @param None
*
* @return None
*
********************************************************************************************************/

void start_time()
{
	clock_gettime(CLOCK_REALTIME, &t1);
	pthread_mutex_lock(&mutex6);
        fp_log = fopen(my_struct->filename, "a");
        fprintf(fp_log, "List_thread: TID: %d, POSIX Thread ID: %d, Start time: %ld.%09ld\n", syscall( __NR_gettid ), pthread_self(), t1.tv_sec, t1.tv_nsec);
        fclose(fp_log);
	fp_log=0;
        pthread_mutex_unlock(&mutex6);
}

/********************************************************************************************************
*
* @name stop_time
* @brief get current time
*
* This function gets the current time with
* a resolution of nanoseconds and logs it to th file
*
* @param None
*
* @return None
*
********************************************************************************************************/

void stop_time()
{
	clock_gettime(CLOCK_REALTIME, &t2);
        pthread_mutex_lock(&mutex7);
        fp_log = fopen(my_struct->filename, "a");
        fprintf(fp_log, "List_thread: TID: %d, POSIX Thread ID: %d, Stop time: %ld.%09ld\n", syscall(__NR_gettid), pthread_self(), t2.tv_sec, t2.tv_nsec);
        fclose(fp_log);
	fp_log=0;
        pthread_mutex_unlock(&mutex7);
}

/********************************************************************************************************
*
* @name signal_handler
* @brief signal handler for USRSIG2
*
* This function handles the SIGUSR2 received from external sources.
* deletes the timer, closes any opened file and safely exits the thread
*
* @param None
*
* @return None
*
********************************************************************************************************/

void signal_handler()
{
	if(thread1)
	{
		if(fp_log)
			fclose(fp_log);
		if(fp_data)
			fclose(fp_data);
		free_function();
		stop_time();
		pthread_cancel(thread1);
		pthread_exit(NULL);
	}
}

/********************************************************************************************************
*
* @name linked_list
* @brief implementation of linked list and data computation
*
* This function implements linked list to calculate the number of times the alphabets 
* has been repeated. Logs the alphabets which repeated exact 3 times to a file.
* This file itself is a thread
*
* @param void *structure of type thread_info
*
* @return None
*
********************************************************************************************************/

void *linked_list(void *structure)
{	
	/*register SIGUSR2*/
	signal(SIGUSR2, signal_handler);	
	my_struct = (struct thread_info *)structure;
	start_time();

	/* allocating memory for nodes of linked list */
	for (i=0; i<NUM_ALPHA; i++)
	{
		list_node[i] = (struct node *) malloc(sizeof(struct node));
		if(!list_node[i])
		{
			printf("Mallocing failed\n");
			exit(-1);
		}
	}

	/* linking every node of the linked list  */
	for(i=0; i<(NUM_ALPHA-1); i++)
	{
		list_node[i]->count = 0;
		list_node[i]->next = list_node[i+1];
	}
	list_node[NUM_ALPHA-1]->count=0;
	list_node[NUM_ALPHA-1]->next=NULL;
	int data;

	/* Open the file */
	fp_data = fopen(my_struct->dataname, "r");
	do
	{
		data = getc(fp_data);
		/* Check for Capital Letters*/
		if(data>64 && data<91)
		{
			/* if present increse the data in the respective node by 1*/
			list_node[data-65]->count++;
		}
		/* check for small letters */
		if(data>96 && data<123)
		{
			list_node[data-97]->count++;
		}
	}while(data!=EOF); /* do until end of file */
	fclose(fp_data);
	fp_data=0;

	/* obtain mutex lock */
	pthread_mutex_lock(&mutex5);
	fp_log = fopen(my_struct->filename, "a");
	/* log the aplhabets which are repeated exactly 3 times */
	fprintf(fp_log, "Alphabets that are mentioned only 3 times\n");
        for(i=0; i<NUM_ALPHA;i++)
	{	
		if(list_node[i]->count==3)
			fprintf(fp_log, "%c\n",i+65);
	}
        fclose(fp_log);
	fp_log=0;
        pthread_mutex_unlock(&mutex5);
	/* free dynamically allocated memory */
	free_function();
	stop_time();
	thread1 = 0;
}

