/********************************************************************************************************
*
* @file linked_lists.h
* @brief contains structure definition and function declaration 
*  for linked list and thread program
*
* This file contains the structure definition and function declarations 
* needed for creating different threads and also for linked list
*
* @author Kiran Hegde
* @date 2/16/2018
* @tools vim editor
*
********************************************************************************************************/


#ifndef LINKED_LISTS_H_

#define LINKED_LISTS_H_

/********************************************************************************************************
*
* Header files
*
********************************************************************************************************/

#include <stdint.h>

/********************************************************************************************************
*
* Macro definition
*
********************************************************************************************************/

#define NUM_ALPHA (26)

/********************************************************************************************************
*
* @name structure thread_info
* @brief contains filename for logging and reading data
*
* This structure contains the filename which is used for logging 
* and also for reading data
*
* @param None
*
* @return None
*
********************************************************************************************************/

struct thread_info
{
	char *filename;
	char *dataname;
};

/********************************************************************************************************
*
* @name structure node
* @brief contains data and next nodes info
*
* This structure defines data parameter and pointer to next node 
* in the linked list
*
* @param None
*
* @return None
*
********************************************************************************************************/

struct node
{
	uint32_t count;
	struct node *next;
};

/********************************************************************************************************
*
* @name linked_list
* @brief thread function to read and compute data from file 
*
* This function is itself a thread which reads data from the file and 
* calculates the alphabets in file
*
* @param void *structure (which is of type struct thread_info)
*
* @return none
*
********************************************************************************************************/

void *linked_list(void *strucutre);

/*********************************************************************************************************
*
* declaring two threads of type pthread_t
*
*
********************************************************************************************************/

pthread_t thread1, thread2;

#endif
