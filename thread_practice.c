/********************************************************************************************************
*
* @file thread_practice.c
* @brief thread creation, computing data and cpu utilization log 
*
* This application creates 2 threads
* One for computing data from a txt file and another for 
* logging cpu utilization at regular intervals
*
* @author Kiran Hegde
* @date 2/17/2018
* @tools vim editor
*
********************************************************************************************************/

/********************************************************************************************************
*
* Header files
*
********************************************************************************************************/

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <sys/syscall.h>
#include <signal.h>
#include "linked_lists.h"

/********************************************************************************************************
*
* defining mutex for locking the filw
*
********************************************************************************************************/
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3 = PTHREAD_MUTEX_INITIALIZER;

/********************************************************************************************************
*
* global declaration of variables
*
********************************************************************************************************/
struct thread_info *my_data;
struct timespec tt1, tt2, tt3;
FILE *file_ptr1, *log_fp1;
uint8_t b=1;
timer_t timer_id;
long double str[7];

/********************************************************************************************************
*
* @name start_time1
* @brief get current time
*
* This function gets the current time with
* a resolution of nanoseconds and logs it to th file
*
* @param None
*
* @return None
*
********************************************************************************************************/

void start_time1()
{
	clock_gettime(CLOCK_REALTIME, &tt1);
	pthread_mutex_lock(&mutex1);
        file_ptr1 = fopen(my_data->filename, "a");
        fprintf(file_ptr1, "Logger_thread: TID: %d, POSIX Thread ID: %d, Start time: %ld.%09ld\n",syscall( __NR_gettid ), pthread_self(), tt1.tv_sec,tt1.tv_nsec);
        fclose(file_ptr1);
        pthread_mutex_unlock(&mutex1);
}

/********************************************************************************************************
*
* @name stop_time1
* @brief get current time
*
* This function gets the current time with
* a resolution of nanoseconds and logs it to th file
*
* @param None
*
* @return None
*
********************************************************************************************************/

void stop_time1()
{
	clock_gettime(CLOCK_REALTIME, &tt2);
        pthread_mutex_lock(&mutex3);
        file_ptr1 = fopen(my_data->filename, "a");
        fprintf(file_ptr1, "Logger_thread: TID: %d, POSIX Thread ID: %d, Stop Time: %ld.%09ld\n",syscall( __NR_gettid), pthread_self(), tt2.tv_sec, tt2.tv_nsec);
        fclose(file_ptr1);
        pthread_mutex_unlock(&mutex3);
}

/********************************************************************************************************
*
* @name signal_handler1
* @brief signal handler for USRSIG1
*
* This function handles the SIGUSR1 received from external sources.
* deletes the timer, closes any opened file and safely exits the thread
*
* @param None
*
* @return None
*
********************************************************************************************************/

void signal_handler1()
{
        timer_delete(timer_id);
        if(log_fp1)
                fclose(log_fp1);
        if(file_ptr1)
                fclose(file_ptr1);
        b=0;
}

/********************************************************************************************************
*
* @name log_func
* @brief logs data to file
*
* This function reads CPU utilization from /proc/stat and 
* writes it to user given file with adding some info
*
* @param None
*
* @return None
*
********************************************************************************************************/

void log_func()
{
	/* Open /proc/stat */
	log_fp1 = fopen("/proc/stat", "r");
        fscanf(log_fp1, "%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf",&str[0], &str[1], &str[2], &str[3], &str[4], &str[5], &str[6], &str[7]);
        file_ptr1 = fopen(my_data->filename, "a");
	/* Obtain the mutex lock */
        pthread_mutex_lock(&mutex2);

	/* Write to file */
        fprintf(file_ptr1, "\nCPU_Utilization\nuser: %Lf, nice: %Lf, system: %Lf, idle: %Lf, iowait: %Lf, irq: %Lf, softirq: %Lf\n", str[1], str[2], str[3], str[4], str[5], str[6], str[7]);
        
	/* unlock the mutex */
	pthread_mutex_unlock(&mutex2);
        
	/* Close the file pointer after operation*/
	fclose(file_ptr1);
	file_ptr1 = 0;
        fclose(log_fp1);
	log_fp1 = 0;
}

/********************************************************************************************************
*
* @name create_timer
* @brief create and initialise a posix timer 
*
* This function creates and initialises a posix timer
* which call the log function for every 100ms
*
* @param None
*
* @return None
*
********************************************************************************************************/

void create_timer()
{

	struct itimerspec ts;
	struct sigevent se;
	int status;

	/* initialising sigevent structure */
	se.sigev_notify = SIGEV_THREAD;
	se.sigev_value.sival_ptr = NULL;
	/* Callback function for the timer*/
	se.sigev_notify_function = &log_func;
	se.sigev_notify_attributes = NULL;

	/* defining timing parameters */
	ts.it_value.tv_sec = 0;
	ts.it_value.tv_nsec = (100000000);
	ts.it_interval.tv_sec = 0;
	ts.it_interval.tv_nsec = (100000000);

	/* create timer using above defined parameters*/
	status = timer_create(CLOCK_REALTIME, &se, &timer_id);
	printf("status1 = %d\n", status);

	/* Set the timer to fire at 100ms */
	status = timer_settime(timer_id, 0, &ts, 0);
	printf("status2 = %d\n", status);

}


/********************************************************************************************************
*
* @name cpu_log
* @brief thread to log cpu utilization
*
* This function itself is a thread which logs CPU utilization to a file
* for every 100ms
*
* @param None
*
* @return None
*
********************************************************************************************************/
void *cpu_log(void *thread)
{
	/* register SIGUSR1 signal */
	signal(SIGUSR1, signal_handler1);
	my_data = (struct thread_info *) thread;
	start_time1();
	create_timer();
	while(b);
	stop_time1();

	/*safely exiting the thread*/
	pthread_exit(NULL);
}

/********************************************************************************************************
*
* @name main function
* @brief creates the thread and waits for it to exit
*
* This function creates two threads and waits for it to exit safely.
*
* @param filename from the command line
*
* @return  on successful execution
*
********************************************************************************************************/
int main(int argc, char *argv[])
{
	printf("Main Started\n");
 	int rc, rc1;
    	long t;
	struct thread_info *ptr;

	/* dynamically allocate memory for the structure*/
	ptr = (struct thread_info *)malloc(sizeof(struct thread_info));
	if(!ptr)
        {
                printf("Exiting\n");
                exit(-1);
        }
	ptr->filename = argv[1];
	ptr->dataname = "Valentinesday.txt";
       	
	/* create thread*/
	rc = pthread_create(&thread1, NULL, linked_list, (void *)ptr);
       	rc1 = pthread_create(&thread2, NULL,cpu_log, (void *)ptr);
	if (rc)
	{
        	printf("ERROR; return code from pthread_create() is %d\n", rc);
        	exit(-1);
       	}
	if (rc1)
        {
                printf("ERROR; return code from pthread_create() is %d\n", rc);
                exit(-1);
        }

	/* wait for threads to exit safely*/
	pthread_join(thread2, NULL);
	pthread_join(thread1, NULL);

	/* free the dynamically allocated memory*/
	free(ptr);
	return 0;
}
